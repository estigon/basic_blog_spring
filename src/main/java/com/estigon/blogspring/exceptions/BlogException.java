package com.estigon.blogspring.exceptions;

import org.springframework.http.HttpStatus;

public class BlogException extends RuntimeException{

    private HttpStatus httpStatus;
    private String message;

    public BlogException(HttpStatus httpStatus, String message) {
        super();
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
