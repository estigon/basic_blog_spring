package com.estigon.blogspring.controllers;

import com.estigon.blogspring.dto.post.PostDTO;
import com.estigon.blogspring.dto.post.ResponsePaginatedPostDTO;
import com.estigon.blogspring.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/post")
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping
    public ResponseEntity<PostDTO> savePosts(@RequestBody PostDTO postDTO){
        return new ResponseEntity(postService.createPost(postDTO), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<ResponsePaginatedPostDTO> getAllPost(Pageable pageable){
        return new ResponseEntity(postService.getAllPost(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostDTO> getPostById(@PathVariable Long id){
        return ResponseEntity.ok(postService.getPostById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PostDTO> updatePost(@PathVariable Long id, @RequestBody PostDTO postDTO){
        return ResponseEntity.ok(postService.updatePost(postDTO, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePost(@RequestParam Long id){
        postService.deletePost(id);
        return ResponseEntity.ok("Post eliminado exitosamente");
    }
}
