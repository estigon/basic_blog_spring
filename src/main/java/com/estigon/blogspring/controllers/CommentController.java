package com.estigon.blogspring.controllers;

import com.estigon.blogspring.dto.comment.CommentDTO;
import com.estigon.blogspring.dto.comment.ResponsePaginatedCommentDTO;
import com.estigon.blogspring.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/post")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping("/{postId}/comments")
    public ResponseEntity<ResponsePaginatedCommentDTO> getPaginatedComments(@PathVariable Long postId, Pageable pageable) {
        return ResponseEntity.ok(commentService.getAllComments(postId, pageable));
    }

    @GetMapping("/{postId}/comments/{id}")
    public ResponseEntity<CommentDTO> getCommentById(@PathVariable Long postId, @PathVariable Long id) {
        return ResponseEntity.ok(commentService.getCommentById(postId, id));
    }

    @PostMapping("/{postId}/comments")
    public ResponseEntity<CommentDTO> createComment(@PathVariable Long postId, @RequestBody CommentDTO commentDTO) {
        return new ResponseEntity(commentService.createComment(postId, commentDTO), HttpStatus.CREATED);
    }


    @PutMapping("/{postId}/comments/{id}")
    public ResponseEntity<CommentDTO> updateComment(@PathVariable Long postId, @PathVariable Long id, @RequestBody CommentDTO commentDTO) {
        return ResponseEntity.ok(commentService.updateComment(postId, commentDTO, id));
    }

    @DeleteMapping("/{postId}/comments/{id}")
    public ResponseEntity<String> deletePost(@PathVariable Long postId, @RequestParam Long id) {
        commentService.deleteComment(postId, id);
        return ResponseEntity.ok("Comment eliminado exitosamente");
    }
}
