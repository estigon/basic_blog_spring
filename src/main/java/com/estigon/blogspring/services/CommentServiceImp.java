package com.estigon.blogspring.services;

import com.estigon.blogspring.dto.comment.CommentDTO;
import com.estigon.blogspring.dto.comment.ResponsePaginatedCommentDTO;
import com.estigon.blogspring.entities.Comment;
import com.estigon.blogspring.entities.Post;
import com.estigon.blogspring.exceptions.BlogException;
import com.estigon.blogspring.exceptions.ResourceNotFoundException;
import com.estigon.blogspring.mappers.CommentMapper;
import com.estigon.blogspring.repositories.CommentRepository;
import com.estigon.blogspring.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CommentServiceImp implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;


    @Override
    public CommentDTO createComment(Long postId, CommentDTO commentDTO) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", postId));

        Comment newComment = CommentMapper.mapToEntity(commentDTO);
        newComment.setPost(post);

        newComment = commentRepository.save(newComment);

        return CommentMapper.mapToDTO(newComment);
    }

    @Override
    public ResponsePaginatedCommentDTO getAllComments(Long postId, Pageable pageable) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", postId));

        Page<Comment> pageComments = commentRepository.findAllByPostId(postId, pageable);

        return CommentMapper.mapToResponsePaginatedDTO(pageComments);
    }

    @Override
    public CommentDTO getCommentById(Long postId, Long id) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", postId));

        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id));

        if(!comment.getPost().getId().equals(post.getId())){
            throw new BlogException(HttpStatus.BAD_REQUEST, "El comentario no pertenece a la publicacion");
        }

        return CommentMapper.mapToDTO(comment);
    }

    @Override
    public CommentDTO updateComment(Long postId, CommentDTO commentDTO, Long id) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", postId));

        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id));

        if(!comment.getPost().getId().equals(post.getId())){
            throw new BlogException(HttpStatus.BAD_REQUEST, "El comentario no pertenece a la publicacion");
        }

        comment.setBody(commentDTO.getBody());
        comment.setEmail(commentDTO.getEmail());
        comment.setName(commentDTO.getName());

        Comment updatedComment = commentRepository.save(comment);

        return CommentMapper.mapToDTO(updatedComment);
    }

    @Override
    public void deleteComment(Long postId, Long id) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", postId));

        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id));

        if(!comment.getPost().getId().equals(post.getId())){
            throw new BlogException(HttpStatus.BAD_REQUEST, "El comentario no pertenece a la publicacion");
        }

        commentRepository.delete(comment);
    }


}
