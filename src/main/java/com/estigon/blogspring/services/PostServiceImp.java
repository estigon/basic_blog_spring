package com.estigon.blogspring.services;

import com.estigon.blogspring.dto.post.PostDTO;
import com.estigon.blogspring.dto.post.ResponsePaginatedPostDTO;
import com.estigon.blogspring.entities.Post;
import com.estigon.blogspring.exceptions.ResourceNotFoundException;
import com.estigon.blogspring.mappers.PostMapper;
import com.estigon.blogspring.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImp implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Override
    public PostDTO createPost(PostDTO postDTO) {
        Post post = PostMapper.mapToEntity(postDTO);
        post = postRepository.save(post);

        return PostMapper.mapToDTO(post);
    }

    @Override
    public PostDTO updatePost(PostDTO postDTO, Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));

        post.setTitle(postDTO.getTitle());
        post.setDescription(postDTO.getDescription());
        post.setContent(postDTO.getContent());

        post = postRepository.save(post);

        return PostMapper.mapToDTO(post);
    }

    @Override
    public ResponsePaginatedPostDTO getAllPost(Pageable pageable) {
        Page<Post> page = postRepository.findAll(pageable);
        return PostMapper.mapToResponsePaginatedDTO(page);
    }

    @Override
    public void deletePost(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));

        postRepository.delete(post);
    }

    @Override
    public PostDTO getPostById(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));

        return PostMapper.mapToDTO(post);
    }

}
