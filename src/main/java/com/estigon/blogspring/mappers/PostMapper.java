package com.estigon.blogspring.mappers;

import com.estigon.blogspring.dto.post.PostDTO;
import com.estigon.blogspring.dto.post.ResponsePaginatedPostDTO;
import com.estigon.blogspring.entities.Post;
import org.springframework.data.domain.Page;

import java.util.stream.Collectors;

public class PostMapper {
    public static PostDTO mapToDTO(Post postEntity) {
        PostDTO postDTO = new PostDTO();
        postDTO.setId(postEntity.getId());
        postDTO.setTitle(postEntity.getTitle());
        postDTO.setDescription(postEntity.getDescription());
        postDTO.setContent(postEntity.getContent());

        return postDTO;
    }

    public static Post mapToEntity(PostDTO postDTO) {
        Post post = new Post();
        post.setId(postDTO.getId());
        post.setTitle(postDTO.getTitle());
        post.setDescription(postDTO.getDescription());
        post.setContent(postDTO.getContent());

        return post;
    }

    public static ResponsePaginatedPostDTO mapToResponsePaginatedDTO(Page<Post> page) {
        ResponsePaginatedPostDTO responsePaginatedPostDTO = new ResponsePaginatedPostDTO();
        responsePaginatedPostDTO.setPosts(
                page.get()
                        .map(post -> mapToDTO(post))
                        .collect(Collectors.toList())
        );

        responsePaginatedPostDTO.setLastPage(page.isLast());
        responsePaginatedPostDTO.setNumberPage(page.getTotalPages());
        responsePaginatedPostDTO.setTotalElements(page.getTotalElements());
        responsePaginatedPostDTO.setNumberPage(page.getNumber());
        responsePaginatedPostDTO.setSizePage(page.getSize());

        return responsePaginatedPostDTO;
    }
}
