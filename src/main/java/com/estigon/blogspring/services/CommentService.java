package com.estigon.blogspring.services;

import com.estigon.blogspring.dto.comment.CommentDTO;
import com.estigon.blogspring.dto.comment.ResponsePaginatedCommentDTO;
import org.springframework.data.domain.Pageable;

public interface CommentService {
    CommentDTO createComment(Long postId, CommentDTO commentDTO);
    ResponsePaginatedCommentDTO getAllComments(Long postId, Pageable pageable);
    CommentDTO getCommentById(Long postId, Long id);
    CommentDTO updateComment(Long postId, CommentDTO commentDTO, Long id);
    void deleteComment(Long postId, Long id);
}
