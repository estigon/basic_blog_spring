package com.estigon.blogspring.services;

import com.estigon.blogspring.dto.post.PostDTO;
import com.estigon.blogspring.dto.post.ResponsePaginatedPostDTO;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface PostService {
    PostDTO createPost(PostDTO postDTO);
    ResponsePaginatedPostDTO getAllPost(Pageable pageable);
    PostDTO getPostById(Long id);
    PostDTO updatePost(PostDTO postDTO, Long id);
    void deletePost(Long id);
}
