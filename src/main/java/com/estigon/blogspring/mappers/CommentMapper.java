package com.estigon.blogspring.mappers;

import com.estigon.blogspring.dto.comment.CommentDTO;
import com.estigon.blogspring.dto.comment.ResponsePaginatedCommentDTO;
import com.estigon.blogspring.entities.Comment;
import org.springframework.data.domain.Page;

import java.util.stream.Collectors;

public class CommentMapper {
    public static Comment mapToEntity(CommentDTO commentDTO) {
        Comment comment = new Comment();
        comment.setId(commentDTO.getId());
        comment.setName(commentDTO.getName());
        comment.setEmail(commentDTO.getEmail());
        comment.setBody(commentDTO.getBody());

        return comment;
    }

    public static CommentDTO mapToDTO(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setName(comment.getName());
        commentDTO.setEmail(comment.getEmail());
        commentDTO.setBody(comment.getBody());

        return commentDTO;
    }

    public static ResponsePaginatedCommentDTO mapToResponsePaginatedDTO(Page<Comment> page) {
        ResponsePaginatedCommentDTO response = new ResponsePaginatedCommentDTO();
        response.setComments(
                page.get().map(comment -> mapToDTO(comment)).collect(Collectors.toList())
        );

        response.setNumberPage(page.getTotalPages());
        response.setSizePage(page.getSize());
        response.setTotalElements(page.getTotalElements());
        response.setLastPage(page.isLast());

        return response;
    }
}
